package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.Laegemiddel;

public class LaegemidelTest {

	@Test
	public void testConstructorLaegemidel() {
		Laegemiddel l = new Laegemiddel("Ketamin", 1, 2, 3, "stk");
		assertEquals(1, l.getEnhedPrKgPrDoegnLet(), 0.001);
		assertEquals(2, l.getEnhedPrKgPrDoegnNormal(), 0.001);
		assertEquals(3, l.getEnhedPrKgPrDoegnTung(), 0.001);
		assertEquals("Ketamin", l.getNavn());
		assertEquals("stk", l.getEnhed());

	}

}
