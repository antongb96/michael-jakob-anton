package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import ordination.DagligFast;
import ordination.Dosis;
import ordination.Laegemiddel;

import org.junit.Test;

public class DagligFastTest {

	@Test
	public void dagligFasttest() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 0, 0, 0, 1);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-25", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());

	}

	@Test
	public void dagligFasttest2() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 20), l, 0, 0, 0, 1);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-20", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());

	}

	@Test
	public void dagligFasttest3() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 10), l, 0, 0, 0, 1);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-10", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());

	}

	@Test
	public void dagligFasttest6() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 0, 0, 0, -1);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-25", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());
		assertEquals(-1, df.doegnDosis(), 0.001);

	}

	@Test
	public void dagligFasttest5() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, -1, 1, 1, 1);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-25", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());
		assertEquals(2, df.doegnDosis(), 0.001);

	}

	@Test
	public void dagligFasttest4() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 1, 2, 3, 4);
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-25", df.getSlutDen().toString());
		assertEquals(l, df.getLaegemiddel());
		assertEquals(10, df.doegnDosis(), 0.001);

	}

	@Test
	public void createDosisTest() {
		Dosis d = new Dosis(LocalTime.of(14, 30), 5);
		assertEquals(LocalTime.of(14, 30), d.getTid());
		assertEquals(5, d.getAntal(), 0.0001);

	}

	@Test
	public void createDosisTest2() {
		Dosis d = new Dosis(LocalTime.of(12, 00), -5);
		assertEquals(LocalTime.of(12, 00), d.getTid());
		assertEquals(-5, d.getAntal(), 0.0001);

	}

	public void samletDosis() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 24), l, 1, 1, 1, 1);
		assertEquals(16, df.samletDosis(), 0.001);

	}

	public void samletDosis2() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 20), l, 2.5, 2.5, 2.5, 2.5);
		assertEquals(10, df.samletDosis(), 0.001);

	}

	public void samletDosis3() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 19), l, 1, 1, 1, 1);
		assertEquals(-4, df.samletDosis(), 0.001);

	}

	public void doegnDosistest1() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 24), l, 1, 1, 1, 1);
		assertEquals(16, df.doegnDosis(), 0.001);

	}

	public void doegnDosistest2() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 24), l, 1, 10, 4, 5);
		assertEquals(20, df.doegnDosis(), 0.001);

	}

	public void doegnDosistest3() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		DagligFast df = new DagligFast(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 24), l, -1, 4, -4, -4);
		assertEquals(-4, df.doegnDosis(), 0.001);

	}

}
