package test;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import ordination.DagligSkaev;
import ordination.Laegemiddel;

import org.junit.Test;

public class DagligSkævTest
{

	@Test
	public void testopret1()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double antalEnheder[] = { 1, 6, 4 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1), l, klokkeSlet,
				antalEnheder);

		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(l, ds.getLaegemiddel());
		assertEquals(3, ds.getDoser().size());
	}

	@Test
	public void testopret2()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double antalEnheder[] = { 1, 6, 4 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2019, 12, 24), l, klokkeSlet,
				antalEnheder);

		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(l, ds.getLaegemiddel());
		assertEquals(3, ds.getDoser().size());
	}

	@Test
	public void testopret3()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double antalEnheder[] = { 1, 6, 4 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 1), l, klokkeSlet,
				antalEnheder);

		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(l, ds.getLaegemiddel());
		assertEquals(3, ds.getDoser().size());
	}

	@Test
	public void testopret4()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double antalEnheder[] = { 1, 6, 4, 6, 7, 3 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 2, 1), l, klokkeSlet,
				antalEnheder);

		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(l, ds.getLaegemiddel());
		assertEquals(3, ds.getDoser().size());
	}

	@Test
	public void testDægnDosis1()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00),
				LocalTime.of(15, 56) };
		double antalEnheder[] = { 4, 3, 4, 10 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 1), l, klokkeSlet,
				antalEnheder);
		assertEquals(21, ds.doegnDosis(), 0.00001);

	}

	@Test
	public void testDægnDosis2()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30) };
		double antalEnheder[] = { 1 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 1), l, klokkeSlet,
				antalEnheder);
		assertEquals(1, ds.doegnDosis(), 0.00001);

	}

	@Test
	public void testDægnDosis3()
	{
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		LocalTime klokkeSlet[] = { LocalTime.of(12, 30), LocalTime.of(13, 30), LocalTime.of(18, 30) };
		double antalEnheder[] = { -2, -5, 2 };

		DagligSkaev ds = new DagligSkaev(LocalDate.of(2020, 1, 1), LocalDate.of(2020, 1, 1), l, klokkeSlet,
				antalEnheder);
		assertEquals(-5, ds.doegnDosis(), 0.00001);
	}
}
