package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.PN;

public class PNTest {

	@Test
	public void pNTest() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 20);
		assertEquals(20, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-25", pn.getSlutDen().toString());
		assertEquals(20, pn.getAntalEnheder(), 0.001);

	}

	@Test
	public void givDosisTest() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 20);
		pn.givDosis(LocalDate.of(2020, 02, 21));
		assertEquals(1, pn.getAntalGangeGivet());

	}

	@Test
	public void doegnDosisTest() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 20);
		pn.givDosis(LocalDate.of(2020, 02, 21));
		assertEquals(3.33, pn.doegnDosis(), 0.1);

	}

	@Test
	public void samletDosisTest() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 20);
		assertEquals(0, pn.samletDosis(), 0.001);

	}

	@Test
	public void samletDosisTest2() {
		Laegemiddel l = new Laegemiddel("Acetylsalicylsyre", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), l, 20);
		pn.givDosis(LocalDate.of(2020, 02, 21));
		assertEquals(20, pn.samletDosis(), 0.001);

	}

}
