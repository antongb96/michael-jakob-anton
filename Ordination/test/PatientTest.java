package test;

import static org.junit.Assert.*;

import java.time.LocalDate;

import ordination.Patient;
import ordination.PN;
import ordination.Laegemiddel;

import org.junit.Test;

public class PatientTest {

	@Test
	public void patientConstructortest() {
		Patient p = new Patient("1212201797", "Henning", 80);
		assertEquals("1212201797", p.getCprnr());
		assertEquals("Henning", p.getNavn());
		assertEquals(80, p.getVaegt(), 0.0001);

	}

	@Test
	public void addordinationTest() {
		Patient p = new Patient("1212201797", "Henning", 80);
		Laegemiddel l = new Laegemiddel("ketamin", 1, 2, 3, "stk");
		PN pn = new PN(LocalDate.of(2020, 02, 24), LocalDate.of(2020, 02, 29), l, 100);
		p.addOrdination(pn);
		assertEquals(true, p.getOrdinationer().contains(pn));

	}

}
