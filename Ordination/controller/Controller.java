package controller;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ordination.DagligFast;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Ordination;
import ordination.PN;
import ordination.Patient;
import storage.Storage;

public class Controller
{
	private Storage storage;
	private static Controller controller;

	private Controller()
	{
		storage = new Storage();
	}

	public static Controller getController()
	{
		if(controller == null)
		{
			controller = new Controller();
		}
		return controller;
	}

	public static Controller getTestController()
	{
		return new Controller();
	}

	/**
	 * Hvis startDato er efter slutDato kastes en IllegalArgumentException og
	 * ordinationen oprettes ikke Pre: startDen, slutDen, patient og laegemiddel er
	 * ikke null
	 * Pre: antal > 0
	 *
	 * @return opretter og returnerer en PN ordination.
	 */
	public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double antal)
	{
		if(antal <= 0)
		{
			throw new IllegalArgumentException("Antal er for lavt");
		}
		if(!checkStartFoerSlut(startDen, slutDen))
			throw new IllegalArgumentException("fejl i dato");
		else if(patient == null)
			throw new IllegalArgumentException("Mangler patient");
		else if(laegemiddel == null)
			throw new IllegalArgumentException("Mangler lægemiddel");
		else
		{
			PN pn = new PN(startDen, slutDen, laegemiddel, antal);
			patient.addOrdination(pn);
			return pn;
		}
	}

	/**
	 * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null,
	 * Pre: morgenAntal, middagAntal, aftenAntal og natAntal >= 0
	 * Pre: morgenAntal + middagAntal + aftenAntal + natAntal >=1
	 */
	public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal,
			double natAntal)
	{
		if(morgenAntal < 0)
			throw new IllegalArgumentException("Kan ikke oprette ordination til " + patient.getNavn()
					+ " fordi morgenAntal er: " + morgenAntal + ". Hvilket er under 0");
		else if(middagAntal < 0)
			throw new IllegalArgumentException("Kan ikke oprette ordination til " + patient.getNavn()
					+ " fordi middagAntal er: " + middagAntal + ". Hvilket er under 0");
		else if(aftenAntal < 0)
			throw new IllegalArgumentException("Kan ikke oprette ordination til " + patient.getNavn()
					+ " fordi aftenAntal er: " + aftenAntal + ". Hvilket er under 0");
		else if(natAntal < 0)
			throw new IllegalArgumentException("Kan ikke oprette ordination til " + patient.getNavn()
					+ " fordi natAntal er: " + natAntal + ". Hvilket er under 0");
		else if(morgenAntal + middagAntal + aftenAntal + natAntal < 1)
			throw new IllegalArgumentException("Der er ikke tilføjet nogen dosis");
		if(!checkStartFoerSlut(startDen, slutDen))
			throw new IllegalArgumentException("📆⏰fejl i dato⏰📆");

		DagligFast df = new DagligFast(startDen, slutDen, laegemiddel, morgenAntal, middagAntal, aftenAntal,
				natAntal);
		patient.addOrdination(df);
		return df;

	}

	/**
	 * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
	 * slutDato kastes en IllegalArgumentException og ordinationen oprettes ikke.
	 * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige kastes
	 * også en IllegalArgumentException.
	 *
	 * Pre: startDen, slutDen, patient og laegemiddel er ikke null
	 */
	public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
			Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder)
	{
		if(!checkStartFoerSlut(startDen, slutDen))
			throw new IllegalArgumentException("fejl i dato");
		else if(antalEnheder.length != klokkeSlet.length)
			throw new IllegalArgumentException("👺KlokkeSlet og antalEnheder stemmer ikke👺");

		DagligSkaev skæver = new DagligSkaev(startDen, slutDen, laegemiddel, klokkeSlet, antalEnheder);
		patient.addOrdination(skæver);
		return skæver;
	}

	/**
	 * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis datoen
	 * ikke er indenfor ordinationens gyldighedsperiode kastes en
	 * IllegalArgumentException Pre: ordination og dato er ikke null
	 */
	public boolean ordinationPNAnvendt(PN ordination, LocalDate dato)
	{
		LocalDate start = ordination.getStartDen();
		LocalDate slut = ordination.getSlutDen();

		if(!checkStartFoerSlut(start, dato) || !checkStartFoerSlut(dato, slut))
			throw new IllegalArgumentException("😈 Fejl i dato 😈");

		return ordination.givDosis(dato);
	}

	/**
	 * Den anbefalede dosis for den pågældende patient (der skal tages hensyn til
	 * patientens vægt). Det er en forskellig enheds faktor der skal anvendes, og
	 * den er afhængig af patientens vægt. Pre: patient og lægemiddel er ikke null
	 */
	public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel)
	{
		double result;
		if(patient.getVaegt() < 25)
		{
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
		}
		else if(patient.getVaegt() > 120)
		{
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
		}
		else
		{
			result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
		}
		return result;
	}

	/**
	 * For et givent vægtinterval og et givent lægemiddel, hentes antallet af
	 * ordinationer. Pre: laegemiddel er ikke null,vægtSlut > 0, vægtStart >= 0,
	 * vægtStart <= vægtSlut
	 */
	public int antalOrdinationerPrVægtPrLægemiddel(double vægtStart, double vægtSlut, Laegemiddel laegemiddel)
	{
		int antal = 0;
		for (Patient patient : getAllPatienter())
		{
			if(patient.getVaegt() >= vægtStart && patient.getVaegt() <= vægtSlut)
			{
				for (Ordination o : patient.getOrdinationer())
				{
					if(o.getLaegemiddel().equals(laegemiddel))
						antal++;
				}
			}
		}
		return antal;
	}

	public List<Patient> getAllPatienter()
	{
		return storage.getAllPatienter();
	}

	public List<Laegemiddel> getAllLaegemidler()
	{
		return storage.getAllLaegemidler();
	}

	/**
	 * Metode der kan bruges til at checke at en startDato ligger før en slutDato.
	 * 
	 * @return true hvis startDato er før slutDato, false ellers.
	 */
	private boolean checkStartFoerSlut(LocalDate startDato, LocalDate slutDato)
	{
		boolean result = true;
		if(slutDato.compareTo(startDato) < 0)
		{
			result = false;
		}
		return result;
	}

	/*
	 * pre: vægt skal være >0 pre: navn og cpr må ikke være null
	 */

	public Patient opretPatient(String cpr, String navn, double vaegt)
	{
		if(vaegt <= 0 || cpr == null || navn == null || cpr.length() != 11)
			throw new IllegalArgumentException("fejl i patient data");
		Patient p = new Patient(cpr, navn, vaegt);
		storage.addPatient(p);
		return p;
	}

	/*
	 * Pre: navn og enhed må ikke være null Pre: enhedPrKgPrDoegnLet,
	 * enhedPrKgPrDoegnNormal og enhedPrKgPrDoegnTung skal være >0
	 */
	public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet,
			double enhedPrKgPrDoegnNormal, double enhedPrKgPrDoegnTung, String enhed)
	{
		if(navn == null || enhed == null || enhedPrKgPrDoegnLet <= 0 || enhedPrKgPrDoegnNormal <= 0
				|| enhedPrKgPrDoegnTung <= 0)
			throw new IllegalArgumentException("Fejl i data");
		Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal,
				enhedPrKgPrDoegnTung, enhed);
		storage.addLaegemiddel(lm);
		return lm;
	}

	public void createSomeObjects()
	{
		opretPatient("121256-0512", "Jane Jensen", 63.4);
		opretPatient("070985-1153", "Finn Madsen", 83.2);
		opretPatient("050972-1233", "Hans Jørgensen", 89.4);
		opretPatient("011064-1522", "Ulla Nielsen", 59.9);
		opretPatient("090149-2529", "Ib Hansen", 87.7);

		opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
		opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
		opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
		opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler().get(1), 123);

		opretPNOrdination(LocalDate.of(2019, 2, 12), LocalDate.of(2019, 2, 14),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler().get(0), 3);

		opretPNOrdination(LocalDate.of(2019, 1, 20), LocalDate.of(2019, 1, 25),
				storage.getAllPatienter().get(3), storage.getAllLaegemidler().get(2), 5);

		opretPNOrdination(LocalDate.of(2019, 1, 1), LocalDate.of(2019, 1, 12),
				storage.getAllPatienter().get(0), storage.getAllLaegemidler().get(1), 123);
		try
		{
			opretDagligFastOrdination(LocalDate.of(2019, 1, 10), LocalDate.of(2019, 1, 12),
					storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);
		} catch (Exception e)
		{
			System.out.println(e.getMessage());
		}

		LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0),
				LocalTime.of(18, 45) };
		double[] an = { 0.5, 1, 2.5, 3 };

		opretDagligSkaevOrdination(LocalDate.of(2019, 1, 23), LocalDate.of(2019, 1, 24),
				storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
	}

}
