package controllerTest;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.Controller;
import ordination.Patient;

public class OpretPatientTest
{

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatientMinus10()
	{

		Patient p = Controller.getController().opretPatient("121212-1234", "Bo Jensen", -10);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatientMinus1()
	{
		Patient p = Controller.getController().opretPatient("121212-1234", "Bo Jensen", -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatient0()
	{

		Patient p = Controller.getController().opretPatient("121212-1234", "Bo Jensen", 0);
	}

	@Test
	public void testopretPatient1()
	{
		Patient p = Controller.getController().opretPatient("121212-1234", "Bo Jensen", 1);
		assertEquals("Bo Jensen", p.getNavn());
		assertEquals("121212-1234", p.getCprnr());
		assertEquals(1, p.getVaegt(), 0.0001);

	}

	@Test
	public void testopretPatient200()
	{
		Patient p = Controller.getController().opretPatient("121212-1234", "Bo Jensen", 200);
		assertEquals("Bo Jensen", p.getNavn());
		assertEquals("121212-1234", p.getCprnr());
		assertEquals(200, p.getVaegt(), 0.0001);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatientCPRfejl()
	{
		Patient p = Controller.getController().opretPatient("1212", "Bo Jensen", 80);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatientNavnNull()
	{

		Patient p = Controller.getController().opretPatient("121212-1234", null, 80);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testopretPatientNullNull()
	{

		Patient p = Controller.getController().opretPatient(null, null, 80);
	}

}
