package controllerTest;

import static org.junit.Assert.*;

import java.time.LocalDate;

import controller.*;
import ordination.PN;
import ordination.Patient;

import org.junit.Test;

public class OrdinationPNAnvendtTest
{

	@Test(expected = IllegalArgumentException.class)
	public void førStart()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.opretPatient("121212-1212", "Lars Larsen", 85);
		PN pn = c.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25), p,
				c.getAllLaegemidler().get(0), 5);
		c.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 19));
	}

	@Test
	public void start()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.opretPatient("121212-1212", "Lars Larsen", 85);
		PN pn = c.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25), p,
				c.getAllLaegemidler().get(0), 5);
		assertEquals(true, c.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 20)));
	}

	@Test
	public void mellem()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.opretPatient("121212-1212", "Lars Larsen", 85);
		PN pn = c.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25), p,
				c.getAllLaegemidler().get(0), 5);
		assertEquals(true, c.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 23)));
	}

	@Test
	public void slut()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.opretPatient("121212-1212", "Lars Larsen", 85);
		PN pn = c.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25), p,
				c.getAllLaegemidler().get(0), 5);
		assertEquals(true, c.ordinationPNAnvendt(pn, LocalDate.of(2020, 02, 25)));
	}

	@Test(expected = IllegalArgumentException.class)
	public void efterSlut()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.opretPatient("121212-1212", "Lars Larsen", 85);
		PN pn = c.opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25), p,
				c.getAllLaegemidler().get(0), 5);
		c.ordinationPNAnvendt(pn, LocalDate.of(2020, 03, 26));
	}
}
