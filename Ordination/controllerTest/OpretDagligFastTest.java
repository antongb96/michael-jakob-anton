package controllerTest;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

import controller.Controller;
import ordination.DagligFast;
import ordination.Laegemiddel;
import ordination.Patient;

public class OpretDagligFastTest {

	@Test
	public void test() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 03, 28), p, l, 1, 1, 1, 1);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-03-28", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test2() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 03, 28), p, l, 0, 0, 0, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-03-28", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void test3() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 03, 28), p, l, 4, 29, 85, 12);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-03-28", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test4() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 03, 28), p, l, -4, 4, 5, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-03-28", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void test5() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 03, 28), p, l, 1, 0, 0, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-03-28", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void test6() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 02, 25), p, l, 4, 4, 1, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-25", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void test7() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2020, 02, 20), p, l, 4, 4, 1, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2020-02-20", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test(expected = IllegalArgumentException.class)
	public void test8() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		DagligFast df = Controller.getController().opretDagligFastOrdination(LocalDate.of(2020, 02, 20),
				LocalDate.of(2010, 01, 10), p, l, 4, 4, 1, 0);
		assertEquals("Acetylsalicylsyre", df.getLaegemiddel().toString());
		assertEquals("2020-02-20", df.getStartDen().toString());
		assertEquals("2010, 01, 10", df.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

}
