package controllerTest;

import static org.junit.Assert.*;

import java.time.LocalDate;
import controller.*;
import ordination.Laegemiddel;
import ordination.PN;
import ordination.Patient;

import org.junit.Test;

public class PNOTest {

	@Test
	public void pNTest() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 10);
		assertEquals(10, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-25", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void pNTest2() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 20), p,
				l, 10);
		assertEquals(10, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-20", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test(expected = IllegalArgumentException.class)
	public void pNTest3() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 10), p,
				l, 10);
		assertEquals(10, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-10", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void pNTest4() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 10);
		assertEquals(10, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-25", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());
	}

	@Test
	public void pNTest5() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 1);
		assertEquals(1, pn.getAntalEnheder(), 0.0001);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-25", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());

	}

	@Test(expected = IllegalArgumentException.class)
	public void pNTest6() {
		Laegemiddel l = Controller.getController().opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "stk");
		Patient p = Controller.getController().opretPatient("250977-1244", "Finn Madsen", 83);
		PN pn = Controller.getController().opretPNOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, 0);
		assertEquals("Acetylsalicylsyre", pn.getLaegemiddel().toString());
		assertEquals("2020-02-20", pn.getStartDen().toString());
		assertEquals("2020-02-25", pn.getSlutDen().toString());
		assertEquals("Acetylsalicylsyre", p.getOrdinationer().get(0).getLaegemiddel().getNavn());

	}
}
