package controllerTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import controller.Controller;
import ordination.Laegemiddel;

public class antalOrdinationerPrVægtPrLægemiddelTest
{

	@Before
	public void setUp() throws Exception
	{
		Controller.getController().createSomeObjects();

	}

	@Test
	public void testantal1Fucidin()
	{
		Controller c = Controller.getController();
		assertEquals(1, c.antalOrdinationerPrVægtPrLægemiddel(50, 80, c.getAllLaegemidler().get(2)));
	}

	@Test
	public void testantal0Fucidin()
	{
		Controller c = Controller.getController();
		assertEquals(0, c.antalOrdinationerPrVægtPrLægemiddel(0, 50, c.getAllLaegemidler().get(2)));
	}

	@Test
	public void testantal2Fucidin()
	{
		Controller c = Controller.getController();
		assertEquals(8, c.antalOrdinationerPrVægtPrLægemiddel(0, 120, c.getAllLaegemidler().get(2)));
	}

	@Test
	public void testantal0Fucidin2()
	{
		Controller c = Controller.getController();
		assertEquals(0, c.antalOrdinationerPrVægtPrLægemiddel(100, 120, c.getAllLaegemidler().get(2)));
	}

	@Test
	public void testantal0Methotrexat()
	{
		Controller c = Controller.getController();
		assertEquals(0, c.antalOrdinationerPrVægtPrLægemiddel(0, 120, c.getAllLaegemidler().get(3)));
	}

}
