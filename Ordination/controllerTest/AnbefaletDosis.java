package controllerTest;

import static org.junit.Assert.*;
import controller.*;
import ordination.Patient;

import org.junit.Test;

public class AnbefaletDosis
{

	@Test
	public void testFucidin()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 83);
		assertEquals(2.075, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(2)), 0.001);
	}

	@Test
	public void testpara()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 83);
		assertEquals(124.5, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(1)), 0.001);
	}

	@Test
	public void testAce()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 83);
		assertEquals(12.45, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}

	@Test
	public void testMetho()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 83);
		assertEquals(1.245, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(3)), 0.001);
	}

	@Test
	public void testacenormal()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 50);
		assertEquals(7.5, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}

	@Test
	public void testacetung()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 121);
		assertEquals(19.36, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}

	@Test
	public void testacelet()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 24);
		assertEquals(2.4, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}

	@Test
	public void testacegrænse1()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 25);
		assertEquals(3.75, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}

	@Test
	public void testacegrænse2()
	{
		Controller c = controller.Controller.getController();
		c.createSomeObjects();

		Patient p = c.opretPatient("123412-1212", "Finn Madsen", 120);
		assertEquals(18, c.anbefaletDosisPrDoegn(p, c.getAllLaegemidler().get(0)), 0.001);
	}
}
