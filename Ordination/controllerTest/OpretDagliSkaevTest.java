package controllerTest;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import controller.Controller;
import ordination.DagligSkaev;
import ordination.Laegemiddel;
import ordination.Patient;

import org.junit.Test;

public class OpretDagliSkaevTest
{

	@Test
	public void test()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double enheder[] = { 1, 6, 4 };
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(20, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, tider, enheder);
		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(true, p.getOrdinationer().contains(ds));
	}

	@Test
	public void test2()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = {};
		double enheder[] = {};
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(20, 02, 20), LocalDate.of(2020, 02, 25), p,
				l, tider, enheder);
		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(true, p.getOrdinationer().contains(ds));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testfejl()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = { LocalTime.of(8, 45), LocalTime.of(9, 00) };
		double enheder[] = { 4 };
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 25),
				p, l, tider, enheder);
	}

	@Test
	public void testDato1()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double enheder[] = { 1, 6, 4 };
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 02, 20),
				p, l, tider, enheder);
		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(true, p.getOrdinationer().contains(ds));
	}

	@Test
	public void testDato2()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double enheder[] = { 1, 6, 4 };
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2020, 03, 25),
				p, l, tider, enheder);
		assertEquals(DagligSkaev.class, ds.getClass());
		assertEquals(true, p.getOrdinationer().contains(ds));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testDatoFejl()
	{
		Controller c = Controller.getController();
		c.createSomeObjects();
		Patient p = c.getAllPatienter().get(1);
		Laegemiddel l = c.getAllLaegemidler().get(0);
		LocalTime tider[] = { LocalTime.of(12, 30), LocalTime.of(13, 45), LocalTime.of(20, 00) };
		double enheder[] = { 1, 6, 4 };
		DagligSkaev ds = c.opretDagligSkaevOrdination(LocalDate.of(2020, 02, 20), LocalDate.of(2010, 01, 10),
				p, l, tider, enheder);
	}

}
