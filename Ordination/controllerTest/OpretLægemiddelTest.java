package controllerTest;

import static org.junit.Assert.*;

import org.junit.Test;
import controller.Controller;
import ordination.Laegemiddel;

public class OpretLægemiddelTest
{

	@Test
	public void testOpretLægemiddel1()
	{
		String navn = "Mebendazol";
		String enhed = "styk";
		Laegemiddel l = Controller.getController().opretLaegemiddel(navn, 1, 2, 3, enhed);
		assertEquals("Mebendazol", l.getNavn());
		assertEquals(1, l.getEnhedPrKgPrDoegnLet(), 0.0001);
		assertEquals(2, l.getEnhedPrKgPrDoegnNormal(), 0.001);
		assertEquals(3, l.getEnhedPrKgPrDoegnTung(), 0.001);
		assertEquals("styk", l.getEnhed());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddel0()
	{

		String navn = "Mebendazol";
		String enhed = "styk";
		Laegemiddel l = Controller.getController().opretLaegemiddel(navn, 0, 2, 3, enhed);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddelMinus1()
	{
		String navn = "Mebendazol";
		String enhed = "styk";
		Laegemiddel l = Controller.getController().opretLaegemiddel(navn, -1, 2, 3, enhed);
	}

	@Test
	public void testOpretLægemiddel321()
	{
		String navn = "Mebendazol";
		String enhed = "styk";
		Laegemiddel l = Controller.getController().opretLaegemiddel(navn, 3, 2, 1, enhed);
		assertEquals("Mebendazol", l.getNavn());
		assertEquals(3, l.getEnhedPrKgPrDoegnLet(), 0.0001);
		assertEquals(2, l.getEnhedPrKgPrDoegnNormal(), 0.001);
		assertEquals(1, l.getEnhedPrKgPrDoegnTung(), 0.001);
		assertEquals("styk", l.getEnhed());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddelp5()
	{
		String navn = "Mebendazol";
		String enhed = "styk";
		Laegemiddel l = Controller.getController().opretLaegemiddel(navn, 124, -1, 200, enhed);
	}

	@Test
	public void testOpretLægemiddelNavnEnhed()
	{
		double enhedlet = 4;
		double enhedmellem = 8;
		double enhedTung = 2;

		Laegemiddel l = Controller.getController().opretLaegemiddel("Mebendazol", enhedlet, enhedmellem,
				enhedTung, "styk");
		assertEquals("Mebendazol", l.getNavn());
		assertEquals(enhedlet, l.getEnhedPrKgPrDoegnLet(), 0.0001);
		assertEquals(enhedmellem, l.getEnhedPrKgPrDoegnNormal(), 0.001);
		assertEquals(enhedTung, l.getEnhedPrKgPrDoegnTung(), 0.001);
		assertEquals("styk", l.getEnhed());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddelNavnnull()
	{
		double enhedlet = 4;
		double enhedmellem = 8;
		double enhedTung = 2;

		Laegemiddel l = Controller.getController().opretLaegemiddel(null, enhedlet, enhedmellem, enhedTung,
				"styk");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddelEnhednull()
	{
		double enhedlet = 4;
		double enhedmellem = 8;
		double enhedTung = 2;

		Laegemiddel l = Controller.getController().opretLaegemiddel("Mebendazol", enhedlet, enhedmellem,
				enhedTung, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testOpretLægemiddelNullNull()
	{
		double enhedlet = 4;
		double enhedmellem = 8;
		double enhedTung = 2;

		Laegemiddel l = Controller.getController().opretLaegemiddel(null, enhedlet, enhedmellem, enhedTung,
				null);
	}

}
