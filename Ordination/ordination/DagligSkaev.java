package ordination;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination
{
	private ArrayList<Dosis> doser = new ArrayList<Dosis>();

	public DagligSkaev(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, LocalTime[] klokkeSlet,
			double[] antalEnheder)
	{
		super(startDen, slutDen, laegemiddel);
		for (int i = 0; i < klokkeSlet.length; i++)
		{
			opretDosis(klokkeSlet[i], antalEnheder[i]);
		}
	}

	private boolean opretDosis(LocalTime tid, double antal)
	{
		Dosis dos = new Dosis(tid, antal);
		return doser.add(dos);
	}

	public ArrayList<Dosis> getDoser()
	{
		return new ArrayList<>(doser);
	}

	@Override
	public double doegnDosis()
	{
		double sum = 0;
		for (Dosis dosis : doser)
		{
			sum += dosis.getAntal();
		}
		return sum;
	}
}
