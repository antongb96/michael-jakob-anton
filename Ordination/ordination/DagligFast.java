package ordination;

import java.time.*;

public class DagligFast extends Ordination
{
	private Dosis[] tider = new Dosis[4];

	/**
	 * @return the tider
	 */
	public Dosis[] getDoser()
	{
		return tider;
	}

	public DagligFast(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double morgenAntal,
			double middagAntal, double aftenAntal, double natAntal)
	{
		super(startDen, slutDen, laegemiddel);

		Dosis morgen = createDosis(LocalTime.of(07, 00), morgenAntal);
		Dosis middag = createDosis(LocalTime.of(12, 00), middagAntal);
		Dosis aften = createDosis(LocalTime.of(18, 00), aftenAntal);
		Dosis nat = createDosis(LocalTime.of(23, 30), natAntal);
		tider[0] = morgen;
		tider[1] = middag;
		tider[2] = aften;
		tider[3] = nat;
	}

	private Dosis createDosis(LocalTime tid, double antal)
	{
		Dosis d = new Dosis(tid, antal);
		return d;
	}

	@Override
	public double samletDosis()
	{
		return super.antalDage() * doegnDosis();
	}

	@Override
	public double doegnDosis()
	{
		double dosis = tider[0].getAntal() + tider[1].getAntal() + tider[2].getAntal() + tider[3].getAntal();
		return dosis;
	}

	@Override
	public String getType()
	{

		return super.getLaegemiddel().getEnhed();
	}
}
