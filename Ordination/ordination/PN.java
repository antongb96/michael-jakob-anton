package ordination;

import java.time.LocalDate;
import java.util.ArrayList;

public class PN extends Ordination
{

	private double antalEnheder;
	private ArrayList<LocalDate> doser = new ArrayList<LocalDate>();

	public PN(LocalDate startDen, LocalDate slutDen, Laegemiddel laegemiddel, double antalEnheder)
	{
		super(startDen, slutDen, laegemiddel);
		this.antalEnheder = antalEnheder;
	}

	/**
	 * Registrerer at der er givet en dosis paa dagen givesDen Returnerer true hvis
	 * givesDen er inden for ordinationens gyldighedsperiode og datoen huskes
	 * Retrurner false ellers og datoen givesDen ignoreres
	 * 
	 * @param givesDen
	 * @return
	 */
	public boolean givDosis(LocalDate givesDen)
	{
		if((givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())
				|| (givesDen.equals(getStartDen()) || givesDen.equals(getSlutDen()))))
		{
			doser.add(givesDen);
			return true;
		}
		else
			return false;
	}

	@Override
	public double doegnDosis()
	{
		return samletDosis() / antalDage();
	}

	@Override
	public double samletDosis()
	{
		return doser.size() * antalEnheder;
	}

	/**
	 * Returnerer antal gange ordinationen er anvendt
	 * 
	 * @return
	 */
	public int getAntalGangeGivet()
	{
		return doser.size();
	}

	public double getAntalEnheder()
	{
		return antalEnheder;
	}

}
